const TrackController = require('../controllers/track.controller');
const verifyApiToken = require('../middlewares/verify-api-token');

const controller = new TrackController();

module.exports = (router) => {
    router.route('/tracks')
    .all(verifyApiToken)
    .get(controller.index.bind(controller))
    .post(controller.create.bind(controller));

    router.route('/tracks/:_id')
    .all(verifyApiToken)
    .put(controller.update.bind(controller))
    .get(controller.show.bind(controller))
    .delete(controller.softDelete.bind(controller));
};
