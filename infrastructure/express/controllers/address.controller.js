const Controller = require('./controller');
const AddressService = require('../../../business/address.service');
const status = require('http-status');

module.exports = class AddressController extends Controller {
    constructor() {
        super(new AddressService());
    }

    async show(req, res, next) {
        try {
            const cep = req.params.cep;
            const address = await this._service.getAddress(cep);
            res.json(address);
        } catch(e) {
            return next(this.buildError('0013', status.NOT_FOUND, e));
        }
    }
};
