const Controller = require('../controllers/controller');
const ModalityService = require('../../../business/modality.service');
const verifyApiToken = require('../middlewares/verify-api-token');

const controller = new Controller(new ModalityService());

module.exports = (router) => {
    router.route('/modalities')
    .all(verifyApiToken)
    .get(controller.index.bind(controller));
};
