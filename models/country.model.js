const { mongoose } = require('../infrastructure/mongoose');

const CountrySchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        maxlength: 100,
        required: true
    }
});

module.exports = mongoose.model('Country', CountrySchema);
