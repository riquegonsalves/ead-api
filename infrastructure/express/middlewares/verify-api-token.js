const status = require('http-status');
const { verifyToken } = require('../../utils/jsonwebtoken');
const UserModel = require('../../../models/user.model');
const UserIpModel = require('../../../models/user-ip.model');
const BlockedAccessModel = require('../../../models/blocked-access.model');

module.exports = async (req, res, next) => {
    const token = req.headers.authorization;
    if (!token) {
        const e = new Error('0009');
        e.statusCode = status.UNAUTHORIZED;
        return next(e);
    }

    try {
        const decoded = await verifyToken(token);
        const user = await UserModel.findById(decoded._id);
        if (!user) {
            const e = new Error('0007');
            e.statusCode = status.NOT_FOUND;
            return next(e);
        }

        try {
            user.verifyCredential(decoded.token_version);
        } catch(e) {
            e.statusCode = status.FORBIDDEN;
            next(e);
            const reason = e.message === '0018' ? 'NOT_LOGGED' : '';
            BlockedAccessModel.register(user, reason);
            return;
        }

        req.user = user;
        UserIpModel.registerIP(req.user, req.ip);
        next();
    } catch(e) {
        e.statusCode = status.UNAUTHORIZED;
        next(e);
    }
};
