const Controller = require('./controller');
const PostService = require('../../../business/post.service');

module.exports = class PostController extends Controller {
    constructor() {
        super(new PostService());
    }

    async index(req, res, next) {
        try {
            const filter = {
                ...req.query,
                ...req.params
            }
            const result = await this._service.find(filter, req.user);
            res.json(result);
        } catch(e) {
            return next(this.buildError(e));
        }
    }
};
