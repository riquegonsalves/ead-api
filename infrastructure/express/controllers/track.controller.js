const Controller = require('./controller');
const TrackService = require('../../../business/track.service');
const status = require('http-status');

module.exports = class TrackController extends Controller {
    constructor() {
        super(new TrackService());
    }
};
