const Controller = require('./controller');
const UserTrackService = require('../../../business/user-track.service');
const status = require('http-status');

module.exports = class TrackController extends Controller {
    constructor() {
        super(new UserTrackService());
    }

    async start(req, res, next) {
        try {
            const result = await this._service.startTrack(req.params._id, req.user);
            res.json(result);
        } catch(e) {
            return next(this.buildError(e));
        }
    }
};
