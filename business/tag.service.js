const Service = require('./service');
const TagModel = require('../models/tag.model');

module.exports = class TagService extends Service {
    constructor() {
        super(TagModel);
    }
};
