const Service = require('./service');
const PostModel = require('../models/post.model');

module.exports = class PostService extends Service {
    constructor() {
        super(PostModel);
    }

    async find(query, user) {
        const filterOptions = this.filterOptions(query);
        query.origin_post = null;
        const posts = await this._model.find(query, null, filterOptions)
            .populate({ path: 'answers', populate: { path: 'author' } }).populate('author');
        const total = await this._model.countDocuments(query);
        return {
            docs: posts,
            total
        };
    }

    async softDelete(_id) {
        const post = await this.findById(_id);
        if(post.origin_post) {
            const origin_post = await this.findById(post.origin_post);
            origin_post.answers = origin_post.answers.filter(c => c !== _id);
            origin_post.save();
        }
        return this.update(_id, {
            deleted: true
        });
    }

    async create(body, user) {
        if (!user.isPremium()) {
            throw new Error('0023');
        }

        const post = await this._model.create({
            ...body,
            author: user
        });

        if(post.origin_post) {
            const parent = await this.findOne({ _id: post.origin_post });
            parent.answers.push(post._id);
            await parent.save();
        }

        return post;
    }
};
