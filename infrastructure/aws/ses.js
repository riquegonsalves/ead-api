const AWS = require('aws-sdk');
AWS.config.update({
    region: 'us-west-2',
    accessKeyId: process.env.AWS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET
});

const ses = new AWS.SES({ apiVersion: '2010-12-01' });

function sendEmail(toAddress, message, subject) {
    const params = {
        Destination: {
            ToAddresses: [ toAddress ]
        },
        Message: {
            Body: {
                Text: {
                    Charset: 'UTF-8',
                    Data: message
                }
            },
            Subject: {
                Charset: 'UTF-8',
                Data: subject
            }
        },
        Source: process.env.SENDER_EMAIL_ADDRESS
    };

    return ses.sendEmail(params).promise();
}

module.exports = sendEmail;
