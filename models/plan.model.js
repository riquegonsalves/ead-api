const { mongoose } = require('../infrastructure/mongoose');

const PlanSchema = new mongoose.Schema({
    _id: {
        type: String,
        trim: true,
        maxlength: 20,
        required: true
    },
    name: {
        type: String,
        trim: true,
        maxlength: 20,
        required: true
    },
    kinds: [{
        duration: {
            type: String,
            enum: ['MONTHLY', 'SEMIANNUALLY', 'ANNUALLY'],
            required: true
        },
        price: {
            type: Number,
            min: 0
        }
    }]
});

module.exports = mongoose.model('Plan', PlanSchema);
