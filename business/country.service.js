const Service = require('./service');
const CountryModel = require('../models/country.model');

module.exports = class CountryService extends Service {
    constructor() {
        super(CountryModel);
    }
};
