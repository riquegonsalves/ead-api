const AuthController = require('../controllers/auth.controller');

const controller = new AuthController();

module.exports = (router) => {
    router.route('/login')
    .post(controller.login.bind(controller));
};
