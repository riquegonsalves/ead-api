const { mongoose } = require('../infrastructure/mongoose');

const UserVideoSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    video: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Video',
        required: true
    },
    status: {
        type: String,
        enum: ['WATCHING', 'DONE'],
        required: true,
        default: 'WATCHING'
    },
    watched: {
        type: Number,
        required: true
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('UserVideo', UserVideoSchema);
