const Controller = require('../controllers/controller');
const TagService = require('../../../business/tag.service');
const verifyApiToken = require('../middlewares/verify-api-token');

const controller = new Controller(new TagService());

module.exports = (router) => {
    router.route('/tags')
    .all(verifyApiToken)
    .get(controller.index.bind(controller));
};
