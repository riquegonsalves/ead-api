const socketIO = require('socket.io');
const { verifyToken } = require('../utils/jsonwebtoken');
const UserModel = require('../../models/user.model');
const userCache = {};

function configSocketIO(server) {
    const io = socketIO(server);

    io.use(async (socket, next) => {
        const token = socket.handshake.headers.authorization;
        if (!token) {
            console.log('connection refused token');
            return next(new Error('authentication error'));
        }

        try{
            const decoded = await verifyToken(token);
            const user = await UserModel.findById(decoded._id);

            if (!user) {
                console.log('connection refused no user');
                return next(new Error('authentication error'));
            }

            try {
                user.verifyCredential(decoded.token_version);
            } catch(e) {
                return next(new Error('authentication error verify credential'));
            }

            socket.user_id = user._id;
            const cached = userCache[user._id];
            console.log('cache before', userCache);
            if (!cached) {
                userCache[user._id] = [socket.id];
            } else if (!cached.includes(socket.id)) {
                const otherConn = io.sockets.connected[cached[0]];
                if (otherConn.handshake.address != socket.handshake.address) {
                    cached.forEach(conn => {
                        console.log('emiting disconnect event');
                        io.sockets.connected[conn].emit('disconnect');
                    });
                    userCache[user._id] = [socket.id];
                } else {
                    cached.push(socket.id);
                }
            }
            console.log('cache after', userCache);
            return next();
        } catch(e) {
            console.log('connection refused', e);
            return next(new Error('authentication error'));
        }
    });

    io.on('connection', async (socket) => {
        console.log('connected', socket.id);
        socket.on('disconnect', () => {
            let cached = userCache[socket.user_id];
            cached = cached.filter(conn => conn !== socket.id);
            if (cached.length) {
                userCache[socket.user_id] = cached;
            } else {
                delete userCache[socket.user_id];
            }
            console.log('disconnected', socket.id);
            console.log('cache after disconnection', userCache);
        });
    });
}

module.exports = configSocketIO;
