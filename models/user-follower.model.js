const { mongoose } = require('../infrastructure/mongoose');

const UserFollowerSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    follower: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

UserFollowerSchema.statics.registerFollower = async function(user, follower) {
    const user_follower = await this.findOne({ user, follower });
    if (!user_follower) {
        await this.create({ user, follower });
    } else {
        throw new Error('0020');
    }
};

module.exports = mongoose.model('UserFollower', UserFollowerSchema);
