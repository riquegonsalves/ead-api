function softDeleteMiddleware(next) {
  var filter = this.getQuery();
  if (filter.deleted == null) {
    filter.deleted = false;
  }
  next();
}

module.exports = { softDeleteMiddleware };
