const OrderController = require('../controllers/order.controller');
const verifyApiToken = require('../middlewares/verify-api-token');

const controller = new OrderController();

module.exports = (router) => {
    router.route('/orders')
    .all(verifyApiToken)
    .post(controller.create.bind(controller))
    .get(controller.index.bind(controller));

    router.route('/orders/:_id')
    .all(verifyApiToken)
    .put(controller.update.bind(controller))
    .get(controller.show.bind(controller));
};
