const schedule = require('node-schedule');
const UserService = require('../../business/user.service');
const userService = new UserService();

const checkPlanExpirationJob = schedule.scheduleJob(
    { hour: 0, minute: 1 },
    async function() {
        try {
            await userService.checkPlanExpiration();
        } catch(e) {
            console.error(e);
        }
    }
);

module.exports = {
    checkPlanExpirationJob
};
