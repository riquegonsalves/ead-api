const { mongoose, softDeleteMiddleware } = require('../infrastructure/mongoose');
const validator = require('validator');

const VideoSchema = new mongoose.Schema({
    url: {
        required: true,
        type: String,
        trim: true,
        validate: validator.isURL
    },
    slug: {
        required: true,
        type: String,
        unique: true
    },
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    title: {
        type: String,
        trim: true,
        maxlength: 100,
        required: true
    },
    duration: {
        type: Number,
        required: true
    },
    summary: {
        type: String,
        trim: true,
        maxlength: 300,
        required: true
    },
    modality: {
        type: String,
        ref: 'Modality',
        required: true
    },
    deleted: {
        type: Boolean,
        required: true,
        default: false
    },
    allowed_roles: [{
        type: String,
        trim: true,
        maxlength: 100
    }],
    tags: [{
        type: String,
        ref: 'Tag',
        required: true
    }],
    like_quantity: {
        type: Number,
        min: 0,
        default: 0
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

VideoSchema.set('toJSON', {
    versionKey: false,
    transform: (model, plain) => {
        delete plain.deleted;
        return plain;
    }
});

VideoSchema.pre('find', softDeleteMiddleware);
VideoSchema.pre('findOne', softDeleteMiddleware);
VideoSchema.pre('countDocuments', softDeleteMiddleware);

module.exports = mongoose.model('Video', VideoSchema);
