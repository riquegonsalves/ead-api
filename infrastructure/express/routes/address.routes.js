const AddressController = require('../controllers/address.controller');

const controller = new AddressController();

module.exports = (router) => {
    router.route('/address/:cep')
    .get(controller.show.bind(controller));
};
