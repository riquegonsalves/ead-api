/*
** Main file to load all migrations from scratch, please start up mongo shell
** on ./migrations folder, so that ./ refers to files on the same folder as
** this file (index.js)
*/

print('loading migrating files');
load('./users.js');
load('./videos.js');
load('./modality.js');
load('./comments.js');

print('migrating teachers ...');
teachersMigration();

print('migrating customers ...');
customerUsersMigration();

print('migrating active users ...');
activeUsersMigration();

print('migrating videos ...');
videoMigration();

print('migrating modalities ...');
modalityMigration();


print('migrating comments ...');
commentsMigration();

print('finished :D');
