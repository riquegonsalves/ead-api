const Service = require('./service');
const TrackModel = require('../models/track.model');
const UserTrackModel = require('../models/user-track.model');
const UserVideoModel = require('../models/user-video.model');

module.exports = class TrackService extends Service {
    constructor() {
        super(UserTrackModel);
    }

    async find(query, user) {
        const userTracks = await this._model.find({ user: user._id }).sort('-updated_at')
        .populate({
            path: 'track',
            populate: { path: 'videos' }
        })
        .lean();

        for(var i = 0; i < userTracks.length; i++) {
            await this.populateUserVideos(userTracks[i]);
        }

        const total = await this._model.countDocuments(query);
        return {
            docs: userTracks,
            total
        };
    }

    async findById(_id) {
        const userTrack = await this._model.findById(_id)
        .populate({
            path: 'track',
            populate: { path: 'videos' }
        })
        .lean();

        if (userTrack) {
            await this.populateUserVideos(userTrack);
        }

        return userTrack;
    }

    async populateUserVideos(userTrack) {
        let userVideos = await UserVideoModel.find({ video: { $in: userTrack.track.videos } }).lean();
        userTrack.track.videos.forEach(video => {
            let userVideo = userVideos.find(e => e.video.toString() == video._id.toString());
            if (userVideo) {
                video.status = userVideo.status;
                video.watched = userVideo.watched;
            }
        });
    }

    async startTrack(_id, user) {
        const track = await TrackModel.findById(_id);
        if (!track) {
            throw new Error('0028');
        }

        const userTrack = await this._model.findOne({
            track: _id,
            user: user._id
        });
        if (userTrack) {
            throw new Error('0029');
        }

        const userVideos = await UserVideoModel.find({
            user: user._id,
            status: 'DONE',
            video: { $in: track.videos }
        });

        const newUserTrack = {
            track: _id,
            user: user._id
        };

        if (userVideos.length == track.videos.length) {
            newUserTrack.status = 'DONE';
            newUserTrack.percentage = 100;
        } else {
            newUserTrack.percentage = parseInt(userVideos.length / track.videos.length * 100);
        }

        return this._model.create(newUserTrack);
    }
};
