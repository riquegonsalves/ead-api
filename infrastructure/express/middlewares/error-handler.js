const status = require('http-status');
const messageDictionary = require('../../utils/message-dictionary');

module.exports = (err, req, res, next) => {
    const message = messageDictionary[err.message];
    const payload = { message: err.message };
    if (message) {
        payload.message = message;
        payload.code = err.message;
    }

    let httpStatus = err.httpStatus;
    if (!httpStatus) {
        if (message) {
            httpStatus = status.BAD_REQUEST;
        } else if (['ValidationError', 'MongoError'].includes(err.name)) {
            httpStatus = status.BAD_REQUEST;
            payload.code = '0001';
        } else {
            httpStatus = status.INTERNAL_SERVER_ERROR;
            payload.code = '9999';
            payload.message = messageDictionary[payload.code];
        }
    }

    console.error(err.originError || err);
    res.status(httpStatus).json(payload);
};
