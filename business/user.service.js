const Service = require('./service');
const UserModel = require('../models/user.model');
const UserPlanModel = require('../models/user-plan.model');
const UserFollowerModel = require('../models/user-follower.model');
const sendEmail = require('../infrastructure/aws/ses');
const { generateToken } = require('../infrastructure/utils/jsonwebtoken');

module.exports = class UserService extends Service {
    constructor() {
        super(UserModel);
    }

    async create(body) {
        const user = await this._model.create(body);
        const action = process.env.EMAIL_VERIFICATION;
        const token = await generateToken({ email: user.email });
        const register_plan = body.register_plan;
        let verificationLink = this.buildLink(action, token, { register_plan });

        //For tests purpose only, remove before finish PR
        console.log(verificationLink);

        try {
            await sendEmail(user.email, verificationLink, 'Verificação de e-mail');
        } catch(e) {
            console.error(e);
            throw new Error('0004');
        }

        return user;
    }

    async find(query, user) {
        let role = query.role;
        if (role && typeof role === 'string') {
            query.role = role.toUpperCase();
        }

        const hasPermission = ['PREMIUM_USER', 'BASIC_USER', 'FREE_USER'].includes(user.role) &&
            ['SENSEI'].includes(query.role);
        if (!hasPermission) {
            throw new Error('0025');
        }
        return this._model.find(query);
    }

    async findByEmail(email) {
        const user = await this.findOne({ email });

        if (!user) {
            throw new Error('0007');
        }

        return user;
    }

    async isAvailable(data) {
        if (Object.keys(data).length > 1) {
            throw new Error('0005');
        }

        if (!data.username && !data.email) {
            throw new Error('0006');
        }

        const user = await this.findOne(data);
        return !user;
    }

    async verifyEmail(email) {
        const user = await this.findByEmail(email);

        if (user.verified_email) {
            throw new Error('0008');
        }

        user.verified_email = true;
        return await this.generateAPIToken(user);
    }

    async updatePassword(email, newPassword) {
        const user = await this.findByEmail(email);
        await user.setPassword(newPassword);
        return await this.generateAPIToken(user);
    }

    async updateLegacyPassword(user, newPassword) {
        user.legacy = false;
        await user.setPassword(newPassword);
        return user.save();
    }

    async sendEmailLink(email) {
        const user = await this.findByEmail(email);
        const action = process.env.EMAIL_VERIFICATION;
        const token = await generateToken({ email: user.email });
        let verificationLink = this.buildLink(action, token);

        //For tests purpose only, remove before finish PR
        console.log(verificationLink);

        await sendEmail(user.email, verificationLink, 'Verificação de e-mail');
        return user;
    }

    async sendPasswordLink(email) {
        const user = await this.findByEmail(email);
        const action = process.env.FORGOT_PASSWORD;
        const token = await generateToken({ email: user.email, new_password: true });
        let forgotPasswordLink = this.buildLink(action, token);

        //For tests purpose only, remove before finish PR
        console.log(forgotPasswordLink);

        await sendEmail(user.email, forgotPasswordLink, 'Troca de senha');
        return user;
    }

    async checkPlanExpiration() {
        const expiredPlans = UserPlanModel.find({
            due_date: {
                $lt: Date.now()
            }
        });

        const expiredUserRoles = [];
        const expiredPlanIds = [];
        expiredPlans.forEach(plan => {
            expiredPlanIds.push(plan._id);
            expiredUserRoles.push(plan.user);
        });

        await UserPlanModel.updateMany(
            { _id: { $in: expiredPlanIds } },
            { $set: { status: 'EXPIRED' } }
        );

        await this._model.updateMany(
            { _id: { $in: expiredUserRoles } },
            { $set: { role: 'FREE_USER' } }
        );
    }

    buildLink(action, token, options = {}) {
        let link = `${process.env.SITE_BASE_URL}${action}`;

        link = `${link}?token=${token}`;

        Object.keys(options).forEach(option => {
            let value = options[option];
            if (value) {
                link = `${link}&${option}=${value}`;
            }
        });

        return link;
    }

    async generateAPIToken(user) {
        user.token_version++;
        user.logged = true;
        await user.save();
        const plainUser = user.toJSON();
        plainUser.apiToken = await generateToken({
            _id: user._id,
            token_version: user.token_version
        });

        return plainUser;
    }

    async follow(_id, user) {
        await UserFollowerModel.registerFollower(_id, user._id);
    }

    async findFollowers(_id) {
        const userFollowers = await UserFollowerModel.find({ user: _id })
        .populate({
            path: 'follower',
            select: 'first_name last_name username'
        });

        return userFollowers.map(userFollower => userFollower.follower);
    }
};
