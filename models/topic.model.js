const { mongoose, softDeleteMiddleware } = require('../infrastructure/mongoose');
const validator = require('validator');

const TopicSchema = new mongoose.Schema({
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    title: {
        type: String,
        trim: true,
        maxlength: 100,
        required: true
    },
    category: {
        type: String,
        ref: 'TopicCategory',
        required: true
    },
    deleted: {
        type: Boolean,
        required: true,
        default: false
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

TopicSchema.set('toJSON', {
    versionKey: false,
    transform: (model, plain) => {
        delete plain.deleted;
        return plain;
    }
});

TopicSchema.pre('find', softDeleteMiddleware);
TopicSchema.pre('findOne', softDeleteMiddleware);
TopicSchema.pre('countDocuments', softDeleteMiddleware);

module.exports = mongoose.model('Topic', TopicSchema);
