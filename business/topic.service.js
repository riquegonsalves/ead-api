const Service = require('./service');
const TopicModel = require('../models/topic.model');

module.exports = class TopicService extends Service {
    constructor() {
        super(TopicModel);
    }

    async find(query, user) {
        const filterOptions = this.filterOptions(query);
        const topics = await this._model.find(query, null, filterOptions)
            .populate('author');
        const total = await this._model.countDocuments(query);
        return {
            docs: topics,
            total
        };
    }

    create(body, user) {
        if (!user.isPremium()) {
            throw new Error('0023');
        }

        return this._model.create({
            ...body,
            author: user
        });
    }
};
