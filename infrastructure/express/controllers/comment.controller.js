const Controller = require('./controller');
const CommentService = require('../../../business/comment.service');
const status = require('http-status');

module.exports = class CommentController extends Controller {
    constructor() {
        super(new CommentService());
    }

    async like(req, res, next) {
        try {
            await this._service.like(req.params._id, req.user);
            res.json(this.buildMessage('0015'));
        } catch(e) {
            return next(this.buildError(e));
        }
    }
};
