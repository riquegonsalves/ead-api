const { mongoose } = require('../infrastructure/mongoose');

const VideoLikeSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    video: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Video',
        required: true
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

VideoLikeSchema.statics.registerLike = async function(user, video) {
    const video_like = await this.findOne({ user, video });
    if (!video_like) {
        await this.create({ user, video });
    } else {
        throw new Error('0019');
    }
};

VideoLikeSchema.statics.removeLike = async function(user, video) {
    const video_like = await this.findOne({ user, video });
    if (video_like) {
        await this.deleteOne({ user, video })
    } else {
        throw new Error('0030');
    }
};

module.exports = mongoose.model('VideoLike', VideoLikeSchema);
