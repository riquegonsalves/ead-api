load('./utils.js');

function parseUser(user) {
    const names = user.name.split(' ');
    const roles = {
        free: 'FREE_USER',
        premium: 'PREMIUM_USER',
        pro: 'SENSEI',
        admin: 'ADMIN',
        moderator: 'FREE_USER',
        adminpro: 'SENSEI',
        affiliator: 'FREE_USER',
        basic: 'BASIC_USER'
    };

    return {
        _id: user._id,
        first_name: names[0],
        last_name: user.name !== names[0] ? names.slice(1).reduce((acc, value) => `${acc} ${value}`): 'Sobrenome',
        username: user.username,
        password: user.hashed_password,
        salt: user.salt,
        legacy: true,
        role: roles[user.role],
        email: user.email,
        verified_email: true,
        created_at: user.register_date,
        updated_at: user.register_date,
        deleted: false,
        logged: false,
        token_version: 1
    }
}

function userMigration(cursor) {
    const totalUsers = iterate(cursor, (user) => {
        db.users.insert(parseUser(user));
    });

    print('Users loaded: ' + totalUsers);
}

function teachersMigration() {
    userMigration(db.users_legacy.find({ teacher: true }));
}

function customerUsersMigration() {
    userMigration(db.users_legacy.find({ role: { $in: ['premium', 'basic'] } , endDate: { $gte: new Date() } }));
}

function activeUsersMigration() {
    userMigration(db.users_legacy.aggregate([{
        $match: {
            role: { $nin :['premium', 'pro', 'admin', 'adminpro', 'basic'] },
            last_login: { $gte: ISODate('2018-01-01') },
            // register_date: { $lt: { $subtract: ['$last_login', 8640000] } }
        }
    }]))
}
