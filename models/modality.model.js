const { mongoose } = require('../infrastructure/mongoose');

const ModalitySchema = new mongoose.Schema({
    _id: {
        type: String,
        trim: true,
        maxlength: 50,
        required: true
    },
    name: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Modality', ModalitySchema);
