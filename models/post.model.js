const { mongoose, softDeleteMiddleware } = require('../infrastructure/mongoose');

const PostSchema = new mongoose.Schema({
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    body: {
        type: String,
        trim: true,
        required: true
    },
    topic: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Topic',
        required: true
    },
    answers: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Post',
        required: true
    }],
    origin_post: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Post',
        required: false
    },
    deleted: {
        type: Boolean,
        required: true,
        default: false
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

PostSchema.pre('find', softDeleteMiddleware);
PostSchema.pre('findOne', softDeleteMiddleware);
PostSchema.pre('countDocuments', softDeleteMiddleware);

module.exports = mongoose.model('Post', PostSchema);
