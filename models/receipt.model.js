const { mongoose } = require('../infrastructure/mongoose');

const ReceiptSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    plan: {
        type: String,
        ref: 'Plan',
        required: true
    },
    order: {
        type: String,
        ref: 'Order',
        required: true
    },
    payment_form: {
        type: String,
        enum: ['MONTHLY', 'SEMIANNUALLY', 'ANNUALLY'],
        required: true
    },
    price: {
        type: Number,
        min: 0,
        required: true
    },
    payment_type: {
        type: String,
        enum: ['CREDIT_CARD', 'POKER_STARS', 'PARTY_POKER', '888_POKER', 'DEPOSIT'],
        required: true
    },
    due_date: {
        type: Date,
        required: true
    },
    transaction_id: {
        type: String
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('Receipt', ReceiptSchema);
