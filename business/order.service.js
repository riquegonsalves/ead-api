const Service = require('./service');
const BraspagService = require('./braspag.service');
const OrderModel = require('../models/order.model');
const UserPlanModel = require('../models/user-plan.model');
const PlanModel = require('../models/plan.model');
const UserModel = require('../models/user.model');
const ReceiptModel = require('../models/receipt.model');
const moment = require('moment');

module.exports = class OrderService extends Service {
    constructor() {
        super(OrderModel);
        this.braspag = new BraspagService();
    }

    find(query, user) {
        const filterOptions = this.filterOptions(query);
        const filter = this.filter(query, user);
        return this._model.find(filter, null, filterOptions);
    }

    async create(body) {
        const creditCard = body.credit_card;
        delete body.credit_card;

        const plan = await PlanModel.findById(body.plan);
        if (plan) {
            const foundKind = plan.kinds.find(kind => kind.duration === body.payment_form);
            body.price = foundKind ? foundKind.price : undefined;
        } else {
            throw new Error('0022');
        }

        const order = await this._model.create(body);

        if (creditCard) {
            const braspagIntegration = await this.braspag.create(order, creditCard);
            order.status = braspagIntegration.status === 201 ? 'APPROVED' : 'REJECTED';
            await order.save();

            if (order.status === 'APPROVED') {
                await this.approvedOrder(order);
            } else {
                throw new Error('0021');
            }
        }

        return order;
    }

    async update(_id, body) {
        const order = await this._model.findById(_id);
        order.status = body.status;
        await order.save();

        if (order.status === 'APPROVED') {
            await this.approvedOrder(order);
        }

        return order;
    }

    async approvedOrder(order) {
        const months = {
            MONTHLY: 1,
            SEMIANNUALLY: 6,
            ANNUALLY: 12
        };
        const due_date = moment().add(months[order.payment_form], 'months');
        await UserPlanModel.create({
            user: order.user,
            plan: order.plan,
            status: 'ACTIVE',
            due_date
        });

        await ReceiptModel.create({
            user: order.user,
            plan: order.plan,
            order,
            payment_form: order.payment_form,
            price: order.price,
            payment_type: order.payment_type,
            due_date
        });

        await UserModel.updateRole(order.user._id, 'PREMIUM_USER');

    }

    filter(query, user) {
        const filter = { user };
        if (query.status && typeof query.status === 'string') {
            filter.status = query.status.toUpperCase();
        }

        if (query.begin_date || query.end_date) {
            filter.created_at = {};
            if (query.begin_date) {
                filter.created_at['$gte'] = query.begin_date;
            }

            if (query.end_date) {
                filter.created_at['$lte'] = query.end_date;
            }
        }

        return filter;
    }
};
