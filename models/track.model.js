const { mongoose, softDeleteMiddleware } = require('../infrastructure/mongoose');

const TrackSchema = new mongoose.Schema({
    title: {
        type: String,
        trim: true,
        maxlength: 100,
        required: true
    },
    videos: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Video',
        required: true
    }],
    level: {
        type: String,
        trim: true,
        maxlength: 100,
        required: true
    },
    icon_url: {
        type: String,
        trim: true,
        maxlength: 300,
        required: true
    },
    deleted: {
        type: Boolean,
        required: true,
        default: false
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

TrackSchema.set('toJSON', {
    versionKey: false,
    transform: (model, plain) => {
        delete plain.deleted;
        return plain;
    }
});

TrackSchema.pre('find', softDeleteMiddleware);
TrackSchema.pre('findOne', softDeleteMiddleware);
TrackSchema.pre('countDocuments', softDeleteMiddleware);

module.exports = mongoose.model('Track', TrackSchema);
