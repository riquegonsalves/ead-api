const Service = require('./service');
const ModalityModel = require('../models/modality.model');

module.exports = class ModalityService extends Service {
    constructor() {
        super(ModalityModel);
    }
};
