const Controller = require('./controller');
const UserService = require('../../../business/user.service');
const UserIpModel = require('../../../models/user-ip.model');
const bcrypt = require('bcrypt');
const sha1 = require('sha1');
const status = require('http-status');

module.exports = class AuthController extends Controller {
    constructor() {
        super(new UserService());
    }

    async login(req, res, next) {
        try {
            const credential = req.body;
            const user = await this._service.findByEmail(credential.login);
            let matched;
            try {
                matched = await this.authenticate(user, credential);
            } catch(e) {
                console.error(e);
                matched = false;
            }

            if (!matched) {
                return next(this.buildError('0014', status.NOT_FOUND));
            }

            if (!user.verified_email) {
                return next(this.buildError('0003', status.FORBIDDEN));
            }

            UserIpModel.registerIP(user, req.ip);
            const payload = await this._service.generateAPIToken(user);
            return res.json(payload);

        } catch(e) {
            return next(this.buildError('0014', status.NOT_FOUND, e));
        }
    }

    async authenticate(user, credential) {
        if (user.legacy) {
            const matched = sha1(credential.password + user.salt) === user.password;
            if (matched) {
                await this._service.updateLegacyPassword(user, credential.password);
            }
            return matched;
        }

        return bcrypt.compare(credential.password, user.password);
    }
};
