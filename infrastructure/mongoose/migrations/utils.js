function iterate(cursor, cb, additionalProps) {
    let count = 0;
    while (cursor.hasNext()) {
        let doc = cursor.next();
        if (additionalProps) {
            doc = Object.assign(doc, additionalProps);
        }
        cb(doc);
        count ++;
    }

    return count;
}
