const { mongoose } = require('../infrastructure/mongoose');
const validator = require('validator');

const MessageSchema = new mongoose.Schema({
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    receiver: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    action: {
        type: String,
        trim: true,
        maxlength: 100,
        required: true
    },
    read: {
        type: Boolean,
        required: true,
        default: false
    },
    kind: {
        type: String,
        enum: ['NOTIFICATION', 'FEED'],
        required: true
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

MessageSchema.statics.createNotification = function(data) {
    return this.create({
        ...data,
        kind: 'NOTIFICATION'
    });
};

MessageSchema.statics.createFeed = function(data) {
    return this.create({
        ...data,
        kind: 'FEED'
    });
};

MessageSchema.statics.markAsRead = function(_id, user_id) {
    return this.updateOne(
        {
            _id,
            receiver: user_id
        },
        { $set: { read: true } },
        { runValidators: true }
    );
};

module.exports = mongoose.model('Message', MessageSchema);
