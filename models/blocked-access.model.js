const { mongoose } = require('../infrastructure/mongoose');

const BlockedAccessSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    reason: {
        type: String,
        enum: ['NOT_LOGGED', 'MISMATCHED_TOKEN_VERSION'],
        default: 'MISMATCHED_TOKEN_VERSION',
        required: true
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

BlockedAccessSchema.statics.register = async function(user, reason) {
    await this.create({ user, reason });
};

module.exports = mongoose.model('BlockedAccess', BlockedAccessSchema);
