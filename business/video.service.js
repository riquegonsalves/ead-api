const Service = require('./service');
const VideoModel = require('../models/video.model');
const VideoLikeModel = require('../models/video-like.model');
const UserVideoModel = require('../models/user-video.model');
const UserFollowerModel = require('../models/user-follower.model');
const UserTrackModel = require('../models/user-track.model');

module.exports = class VideoService extends Service {
    constructor() {
        super(VideoModel);
    }

    async find(query, user) {
        const filterOptions = this.filterOptions(query);
        const videos = await this._model.find(query, null, filterOptions).populate('author modality', 'first_name last_name username photo name');
        videos.forEach(video => this.controlAccess(video, user.role));
        const total = await this._model.countDocuments(query);
        return {
            docs: videos,
            total
        };
    }

    async findFollowed(query, user) {
        let followed = await UserFollowerModel.find({ follower: user._id });
        if (followed.length) {
            followed = followed.map(user_follower => user_follower.user);
        } else {
            const pipeline = [{ $group: { _id: "$user", count: { $sum: 1 } } }];
            followed = await UserFollowerModel.aggregate(pipeline);
            const first = followed[0];
            followed = first ? [ first._id ] : [];
        }

        query.author = { $in: followed };
        return this.find(query, user);
    }

    async advancedFind(query, user) {
        const and = [];
        if (query.search) {
            const search = query.search;
            const regex = RegExp(`${search}`, 'i') ;
            and.push({
                $or: [
                    { title: regex },
                    { summary: regex }
                ]
            });
            delete query.search;
        }

        const or = [];
        if (query.tags) {
            or.push({ tags: { $in: query.tags } });
            delete query.tags
        }

        if(query.modality) {
            or.push({ modality: { $in: query.modality } });
            delete query.modality
        }

        if(or.length) {
            and.push({ $or: or });
        }

        if (and.length) {
            query.$and = and;
        }

        return this.find(query, user);
    }

    async findById(_id, user) {
        const video = await this._model.findOne({ _id })
        .populate({
            path: 'author',
            select: 'first_name last_name username'
        })
        .lean();
        await this.populateUserVideo(video, user._id);
        this.controlAccess(video, user.role);
        const liked = await VideoLikeModel.findOne({ user: user._id, video: _id }) ? true : false;
        const result = { ...video, liked }
        return result;
    }

    async populateUserVideo(video, user_id) {
        let userVideo = await UserVideoModel.findOne({ user: user_id, video: video._id });
        if (userVideo) {
            video.status = userVideo.status;
            video.watched = userVideo.watched;
        }
    }

    controlAccess(video, role) {
        if (!['FREE_USER', 'PREMIUM_USER'].includes(role)) {
            return;
        }

        if (video && !video.allowed_roles.includes(role)) {
            video.url = "";
        }
    }

    async like(_id, user) {
        await VideoLikeModel.registerLike(user._id, _id);
        await this._model.update({ _id }, { $inc: { like_quantity: 1 } });
    }

    async unlike(_id, user) {
        await VideoLikeModel.removeLike(user._id, _id);
        await this._model.update({ _id }, { $inc: { like_quantity: -1 } });
    }

    async updateWatched({ _id, watched }, user) {
        const video = await this._model.findById(_id);
        const userVideo = await UserVideoModel.findOne({
            user: user._id,
            video
        });

        if (video && video.allowed_roles.includes(user.role)) {
            watched = parseInt(watched);
            if (!watched || watched > video.duration) {
                throw new Error('0027');
            }

            if (userVideo && userVideo.status === 'WATCHING') {
                userVideo.watched = watched;

                if (watched == video.duration) {
                    userVideo.status = 'DONE';
                    await userVideo.save();
                    UserTrackModel.updateTracks(userVideo.user, userVideo.video);
                    return userVideo;
                }

                return userVideo.save();
            }

            return UserVideoModel.create({
                user: user._id,
                video,
                watched
            });
        } else {
            throw new Error('0026');
        }
    }
};
