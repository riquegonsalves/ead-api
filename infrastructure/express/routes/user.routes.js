const UserController = require('../controllers/user.controller');
const verifyApiToken = require('../middlewares/verify-api-token');

const controller = new UserController();

module.exports = (router) => {
    router.route('/users')
    .post(controller.create.bind(controller))
    .get(verifyApiToken, controller.index.bind(controller))
    .put(verifyApiToken, controller.update.bind(controller));

    router.route('/users/available')
    .get(controller.check.bind(controller));

    router.route('/users/verified_email')
    .put(controller.verifyEmail.bind(controller));

    router.route('/users/new_verified_email')
    .post(controller.resendEmailVerification.bind(controller));

    router.route('/users/forgot_password')
    .post(controller.sendForgotPasswordLink.bind(controller));

    router.route('/users/new_password')
    .put(controller.changePassword.bind(controller));

    router.route('/me')
    .get(verifyApiToken, controller.show.bind(controller));

    router.route('/users/:_id/followers')
    .all(verifyApiToken)
    .post(controller.follow.bind(controller))
    .get(controller.listFollowers.bind(controller))
};
