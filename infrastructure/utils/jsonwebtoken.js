const jwt = require('jsonwebtoken');
const secret = process.env.API_PRIVATE_KEY || 'private-key';

function callback(error, data) {
    return error ? Promise.reject(error) : Promise.resolve(data);
}

function generateToken(payload, expiresIn = '7d') {
    return new Promise((resolve, reject) => {
        const options = { expiresIn };
        jwt.sign(payload, secret, options, (error, token) => {
            error ? reject(error) : resolve(token);
        });
    });
}

function verifyToken(token) {
    return new Promise((resolve, reject) => {
        jwt.verify(token, secret, (error, decoded) => {
            error ? reject(error) : resolve(decoded);
        });
    });
}

module.exports = { generateToken, verifyToken };
