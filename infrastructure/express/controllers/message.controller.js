const Controller = require('./controller');
const MessageService = require('../../../business/message.service');
const status = require('http-status');

module.exports = class MessageController extends Controller {
    constructor() {
        super(new MessageService());
    }

    async listFeeds(req, res, next) {
        try {
            const result = await this._service.listFeeds(req.user);
            res.json(result);
        } catch(e) {
            return next(this.buildError(e));
        }
    }

    async listNotifications(req, res, next) {
        try {
            const result = await this._service.listNotifications(req.user);
            res.json(result);
        } catch(e) {
            return next(this.buildError(e));
        }
    }

    async markAsRead(req, res, next) {
        try {
            await this._service.markAsRead(req.params._id, req.user);
            res.json(this.buildMessage('0015'));
        } catch(e) {
            return next(this.buildError(e));
        }
    }
};
