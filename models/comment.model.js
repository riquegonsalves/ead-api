const { mongoose, softDeleteMiddleware } = require('../infrastructure/mongoose');

const CommentSchema = new mongoose.Schema({
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    body: {
        type: String,
        trim: true,
        required: true
    },
    video: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Video',
        required: true
    },
    answers: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Comment',
        required: true
    }],
    origin_comment: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Comment',
        required: false
    },
    like_quantity: {
        type: Number,
        min: 0,
        default: 0
    },
    deleted: {
        type: Boolean,
        required: true,
        default: false
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

CommentSchema.pre('find', softDeleteMiddleware);
CommentSchema.pre('findOne', softDeleteMiddleware);
CommentSchema.pre('countDocuments', softDeleteMiddleware);

module.exports = mongoose.model('Comment', CommentSchema);
