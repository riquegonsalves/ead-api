const { mongoose } = require('../infrastructure/mongoose');

const OrderSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    plan: {
        type: String,
        ref: 'Plan',
        required: true
    },
    payment_form: {
        type: String,
        enum: ['MONTHLY', 'SEMIANNUALLY', 'ANNUALLY'],
        required: true
    },
    price: {
        type: Number,
        min: 1,
        required: true
    },
    installments: {
        type: Number,
        min: 1,
        default: 1
    },
    payment_type: {
        type: String,
        enum: ['CREDIT_CARD', 'POKER_STARS', 'PARTY_POKER', '888_POKER', 'DEPOSIT'],
        required: true
    },
    status: {
        type: String,
        enum: ['PENDING', 'APPROVED', 'CANCELED', 'REJECTED'],
        default: 'PENDING'
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

module.exports = mongoose.model('Order', OrderSchema);
