const Service = require('./service');
const BraspagIntegrationModel = require('../models/braspag-integration.model');
const axios = require('axios');

module.exports = class BraspagService extends Service {
    constructor() {
        super(BraspagIntegrationModel);
    }

    async create(order, creditCard) {
        const url = process.env.BRASPAG_SALE_URL;

        const config = {
            headers: {
                "MerchantId": process.env.BRASPAG_MERCHANT_ID,
                "MerchantKey": process.env.BRASPAG_MERCHANT_KEY
            }
        };

        const braspagRequest = {
            "MerchantOrderId": order.id,
            "Customer": {
                "Name": order.user.name
            },
            "Payment": {
                "Provider": process.env.BRASPAG_CREDIT_CARD_PROVIDER,
                "Type": "CreditCard",
                "Capture": true,
                "Amount": order.price,
                "Installments": order.installments,
                "CreditCard": {
                    "CardNumber": creditCard.number,
                    "Holder": creditCard.holder,
                    "ExpirationDate": creditCard.expiration_date,
                    "SecurityCode": creditCard.security_code,
                    "Brand": creditCard.brand
                }
            }
        };

        if (order.payment_form === 'MONTHLY') {
            braspagRequest["Payment"]["RecurrentPayment"] = {
                "AuthorizeNow": true,
                "Interval": "Monthly"
            };
            braspagRequest["Payment"]["Installments"] = 1;
        }

        let response;
        try {
            response = await axios.post(url, braspagRequest, config);
        } catch(e) {
            response = e.response;
        }

        return this._model.register(
            order, braspagRequest, response.data, response.status
        );
    }
};
