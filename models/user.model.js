const { mongoose, softDeleteMiddleware } = require('../infrastructure/mongoose');
const bcrypt = require('bcrypt');
const validator = require('validator');

const AddressSchema = new mongoose.Schema({
    country: {
        type: String,
        trim: true,
        maxlength: 100,
        required: true
    },
    cep: {
        type: String,
        trim: true,
        maxlength: 8,
        required: true
    },
    city: {
        type: String,
        trim: true,
        maxlength: 100,
        required: true
    },
    neighborhood: {
        type: String,
        trim: true,
        maxlength: 100,
        required: true
    },
    street: {
        type: String,
        trim: true,
        maxlength: 100,
        required: true
    },
    number: {
        type: String,
        trim: true,
        maxlength: 100,
        required: true
    }
});

const UserSchema = new mongoose.Schema({
    first_name: {
        type: String,
        trim: true,
        maxlength: 15,
        required: true
    },
    last_name: {
        type: String,
        trim: true,
        maxlength: 15,
        required: true
    },
    email: {
        required: true,
        type: String,
        trim: true,
        validate: validator.isEmail,
        unique: true
    },
    verified_email: {
        required: true,
        type: Boolean,
        default: false
    },
    password: {
        type: String,
        required: true
    },
    salt: {
        type: String
    },
    username: {
        type: String,
        trim: true,
        maxlength: 20,
        required: true,
        unique: true
    },
    role: {
        type: String,
        enum: ['FREE_USER', 'BASIC_USER', 'PREMIUM_USER', 'SENSEI', 'ADMIN'],
        required: true,
        default: 'FREE_USER'
    },
    deleted: {
        type: Boolean,
        required: true,
        default: false
    },
    logged: {
        type: Boolean,
        required: true,
        default: false
    },
    legacy: {
        type: Boolean,
        required: true,
        default: false
    },
    token_version: {
        type: Number,
        required: true,
        default: 1
    },
    photo: {
        type: String
    },
    address: AddressSchema
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

UserSchema.pre('save', async function() {
    if (this.isNew) {
        await this.setPassword(this.password);
        this.verified_email = false;
    }
});

UserSchema.statics.updateRole = function(_id, newRole) {
    return this.updateOne(
        { _id },
        { $set: { role: newRole } },
        { runValidators: true }
    );
};

UserSchema.methods.setPassword = async function(newPassword) {
    const SALT_ROUNDS = 10;
    this.salt = await bcrypt.genSalt(10);
    this.password = await bcrypt.hash(newPassword, this.salt);
};

UserSchema.methods.isPremium = async function() {
    return this.role === 'PREMIUM_USER';
};

UserSchema.methods.isAdmin = async function() {
    return this.role === 'ADMIN'
};

UserSchema.virtual('name').get(function () {
  return `${this.first_name} ${this.last_name}`;
});

UserSchema.methods.verifyCredential = function(token_version) {
    if (!token_version || token_version != this.token_version) {
        throw new Error('0017');
    }

    if (!this.logged) {
        throw new Error('0018');
    }
};

UserSchema.set('toJSON', {
    versionKey: false,
    transform: (model, plain) => {
        const omittedFields = [
            'salt',
            'password',
            'verified_email',
            'deleted',
            'token_version',
            'logged'
        ];

        omittedFields.forEach(field => {
            delete plain[field];
        })

        return plain;
    }
});

UserSchema.pre('find', softDeleteMiddleware);
UserSchema.pre('findOne', softDeleteMiddleware);


module.exports = mongoose.model('User', UserSchema);
