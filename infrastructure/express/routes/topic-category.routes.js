const Controller = require('../controllers/controller');
const TopicCategoryService = require('../../../business/topic-category.service');
const verifyApiToken = require('../middlewares/verify-api-token');

const controller = new Controller(new TopicCategoryService());

module.exports = (router) => {
    router.route('/topic_categories')
    .all(verifyApiToken)
    .get(controller.index.bind(controller));
};
