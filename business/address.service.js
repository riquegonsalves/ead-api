const Service = require('./service');
const UserModel = require('../models/user.model');
const cepPromise = require('cep-promise');

module.exports = class AddressService extends Service {
    constructor() {
        super();
    }

    getAddress(cep) {
        return cepPromise(cep);
    }
};
