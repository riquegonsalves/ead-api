const admin = require('firebase-admin');

const serviceAccount = require('./service-account');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://sensei-poker.firebaseio.com"
});

module.exports = admin;
