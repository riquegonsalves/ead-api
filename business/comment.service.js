const Service = require('./service');
const CommentModel = require('../models/comment.model');
const CommentLikeModel = require('../models/comment-like.model');

module.exports = class CommentService extends Service {
    constructor() {
        super(CommentModel);
    }

    async find(query, user) {
        const filterOptions = this.filterOptions(query);
        query.origin_comment = null;
        const comments = await this._model.find(query, null, filterOptions)
            .populate({ path: 'answers', populate: { path: 'author' } }).populate('author');
        const total = await this._model.countDocuments(query);
        return {
            docs: comments,
            total
        };
    }

    async softDelete(_id) {
        const comment = await this.findById(_id);
        if(comment.origin_comment) {
            const origin_comment = await this.findById(comment.origin_comment);
            origin_comment.answers = origin_comment.answers.filter(c => c !== _id);
            origin_comment.save();
        }
        return this.update(_id, {
            deleted: true
        });
    }

    async create(body, user) {
        if (!user.isPremium()) {
            throw new Error('0023');
        }

        const comment = await this._model.create({
            ...body,
            author: user
        });

        if(comment.origin_comment) {
            const parent = await this.findOne({ _id: comment.origin_comment });
            parent.answers.push(comment._id);
            await parent.save();
        }

        return comment;
    }

    async like(_id, user) {
        await CommentLikeModel.registerLike(user._id, _id);
        await this._model.update({ _id }, { $inc: { like_quantity: 1 } });
    }
};
