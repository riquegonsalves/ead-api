const UserTrackController = require('../controllers/user-track.controller');
const verifyApiToken = require('../middlewares/verify-api-token');

const controller = new UserTrackController();

module.exports = (router) => {
    router.route('/users/tracks')
    .all(verifyApiToken)
    .get(controller.index.bind(controller));

    router.route('/users/tracks/:_id')
    .all(verifyApiToken)
    .get(controller.show.bind(controller))
    .post(controller.start.bind(controller));
};
