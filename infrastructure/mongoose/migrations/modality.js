load('./utils.js');

function parseModality(modality) {
    return {
        _id: modality.slug,
        name: modality.title
    }
}

function insertModality(modality) {
    db.modalities.insert(parseModality(modality))
    db.videos.updateMany({ _id: { $in: modality.topics } }, { $set: { modality: modality.slug }})
}

function modalityMigration() {
    const cursor = db.subcategories_legacy.find();
    const totalModalities = iterate(cursor, insertModality);

    print('Modalities loaded: ' + totalModalities);
}
