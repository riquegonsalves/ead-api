const { mongoose } = require('../infrastructure/mongoose');

const UserPlanSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    plan: {
        type: String,
        ref: 'Plan',
        required: true
    },
    due_date: {
        type: Date,
        required: true
    },
    begin_date: {
        type: Date,
        required: true,
        default: Date.now
    },
    status: {
        type: String,
        enum: ['ACTIVE', 'INACTIVE', 'EXPIRED'],
        required: true
    }
});

module.exports = mongoose.model('UserPlan', UserPlanSchema);
