const { mongoose } = require('../infrastructure/mongoose');
const TrackModel = require('./track.model');
const UserVideoModel = require('./user-video.model');

const UserTrackSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    track: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Track',
        required: true
    },
    percentage: {
        type: Number,
        required: true,
        default: 0
    },
    status: {
        type: String,
        enum: ['STARTED', 'DONE'],
        required: true,
        default: 'STARTED'
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

UserTrackSchema.statics.updateTracks = async function(user, video) {
    const tracks = await TrackModel.find({ videos: video }).populate('videos');
    if (tracks.length) {
        const userTracks = await this.find({
            status: 'STARTED',
            user,
            track: { $in: tracks.map(track => track._id) }
        });

        if (userTracks) {
            userTracks.forEach(async userTrack => {
                const track = tracks.find(e => e._id.toString() == userTrack.track.toString());
                const userVideos = await UserVideoModel.find({
                    status: 'DONE',
                    video: { $in: track.videos }
                });

                const totalVideos = track.videos.length;
                const videosWatched = userVideos.length;
                if (totalVideos == videosWatched) {
                    userTrack.percentage = 100;
                    userTrack.status = 'DONE';
                } else {
                    userTrack.percentage = parseInt( videosWatched / totalVideos * 100 );
                }

                userTrack.save();
            });
        }

    }
};

module.exports = mongoose.model('UserTrack', UserTrackSchema);
