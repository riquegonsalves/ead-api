const Service = require('./service');
const PlanModel = require('../models/plan.model');

module.exports = class PlanService extends Service {
    constructor() {
        super(PlanModel);
    }
};
