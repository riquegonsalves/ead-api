if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}
const app = require('./infrastructure/express');
require('./infrastructure/jobs');
const http = require('http');
const PORT = process.env.PORT || 4000;
const server = http.createServer(app);
require('./infrastructure/socket-io')(server);

server.listen(PORT, () => {
  console.log(`App running on ${PORT}`);
});
