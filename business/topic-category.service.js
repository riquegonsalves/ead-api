const Service = require('./service');
const TopicCategoryModel = require('../models/topic-category.model');

module.exports = class TopicCategoryService extends Service {
    constructor() {
        super(TopicCategoryModel);
    }
};
