const MessageController = require('../controllers/message.controller');
const verifyApiToken = require('../middlewares/verify-api-token');

const controller = new MessageController();

module.exports = (router) => {
    router.route('/feeds')
    .all(verifyApiToken)
    .get(controller.listFeeds.bind(controller));

    router.route('/feeds/:_id')
    .all(verifyApiToken)
    .put(controller.markAsRead.bind(controller));
};
