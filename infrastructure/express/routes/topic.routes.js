const TopicController = require('../controllers/topic.controller');
const verifyApiToken = require('../middlewares/verify-api-token');

const controller = new TopicController();

module.exports = (router) => {
    router.route('/topics')
    .all(verifyApiToken)
    .get(controller.index.bind(controller))
    .post(controller.create.bind(controller));

    router.route('/topics/:_id')
    .all(verifyApiToken)
    .put(controller.update.bind(controller))
    .get(controller.show.bind(controller))
    .delete(controller.softDelete.bind(controller));
};
