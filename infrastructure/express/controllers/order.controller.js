const Controller = require('./controller');
const OrderService = require('../../../business/order.service');
const status = require('http-status');

module.exports = class OrderController extends Controller {
    constructor() {
        super(new OrderService());
    }

    async create(req, res, next) {
        try {
            req.body.user = req.user;
            const result = await this._service.create(req.body);
            res.json(result);
        } catch(e) {
            return next(this.buildError(e));
        }
    }
};
