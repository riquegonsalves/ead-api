const Service = require('./service');
const MessageModel = require('../models/message.model');

module.exports = class MessageService extends Service {
    constructor() {
        super(MessageModel);
    }

    markAsRead(_id, user) {
        return this._model.markAsRead(_id, user);
    }

    listFeeds(user) {
        const query = {
            kind: 'FEED',
            receiver: user._id,
            read: false
        };
        return this._model.find(query, null, { sort: '-created_at' });
    }

    listNotifications(user) {
        const query = {
            kind: 'NOTIFICATION',
            receiver: user._id,
            read: false
        };
        return this._model.find(query, null, { sort: '-created_at' });
    }
};
