const { mongoose } = require('../infrastructure/mongoose');

const TagSchema = new mongoose.Schema({
    _id: {
        type: String,
        trim: true,
        maxlength: 50,
        required: true
    }
});

module.exports = mongoose.model('Tag', TagSchema);
