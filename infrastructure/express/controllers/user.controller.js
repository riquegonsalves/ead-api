const Controller = require('./controller');
const UserService = require('../../../business/user.service');
const { generateToken, verifyToken } = require('../../utils/jsonwebtoken');
const status = require('http-status');

module.exports = class UserController extends Controller {
    constructor() {
        super(new UserService());
    }

    async show(req, res, next) {
        try {
            const result = await this._service.findById(req.user._id);
            res.json(result);
        } catch(e) {
            return next(this.buildError(e));
        }
    }

    async update(req, res, next) {
        try {
            await this._service.update(req.user._id, req.body);
            res.json(this.buildMessage('0015'));
        } catch(e) {
            return next(this.buildError(e));
        }
    }

    async check(req, res, next) {
        try {
            const available = await this._service.isAvailable(req.query);
            res.json({ available });
        } catch(e) {
            return next(this.buildError(e));
        }
    }

    async verifyEmail(req, res, next) {
        try {
            const token = req.query.token;
            if (!token) {
                throw new Error('0009');
            }

            const decoded = await verifyToken(token);
            if (!decoded.email && !decoded.new_password) {
                throw new Error('0010');
            }

            const payload = await this._service.verifyEmail(decoded.email);
            res.json(payload);
        } catch(e) {
            return next(this.buildError(e));
        }
    }

    async resendEmailVerification(req, res, next) {
        try {
            const email = req.body.email;
            if (!email) {
                throw new Error('0011');
            }

            await this._service.sendEmailLink(email);
            res.json(this.buildMessage('0015'));
        } catch(e) {
            return next(this.buildError(e));
        }
    }

    async sendForgotPasswordLink(req, res, next) {
        try {
            const email = req.body.email;
            if (!email) {
                throw new Error('0011');
            }

            await this._service.sendPasswordLink(email);
            res.json(this.buildMessage('0015'));
        } catch(e) {
            return next(this.buildError(e));
        }
    }

    async changePassword(req, res, next) {
        try {
            const token = req.query.token;
            if (!token) {
                throw new Error('0009');
            }

            const decoded = await verifyToken(token);
            if (!decoded.email && !decoded.new_password) {
                throw new Error('0010');
            }

            const newPassword = req.body.new_password
            if (!newPassword) {
                throw new Error('0012');
            }

            const payload = await this._service.updatePassword(decoded.email, newPassword);
            res.json(payload);
        } catch(e) {
            return next(this.buildError(e));
        }
    }

    async follow(req, res, next) {
        try {
            await this._service.follow(req.params._id, req.user);
            res.json(this.buildMessage('0015'));
        } catch(e) {
            return next(this.buildError(e));
        }
    }

    async listFollowers(req, res, next) {
        try {
            const result = await this._service.findFollowers(req.params._id);
            res.json(result);
        } catch(e) {
            return next(this.buildError(e));
        }
    }
};
