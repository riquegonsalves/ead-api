const Controller = require('../controllers/controller');
const CountryService = require('../../../business/country.service');

const controller = new Controller(new CountryService());

module.exports = (router) => {
    router.route('/countries')
    .get(controller.index.bind(controller));
};
