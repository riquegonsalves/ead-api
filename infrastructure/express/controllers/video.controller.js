const Controller = require('./controller');
const VideoService = require('../../../business/video.service');
const status = require('http-status');

module.exports = class VideoController extends Controller {
    constructor() {
        super(new VideoService());
    }

    async create(req, res, next) {
        try {
            req.body.author = req.user;
            const result = await this._service.create(req.body);
            res.json(result);
        } catch(e) {
            return next(this.buildError(e));
        }
    }

    async followedIndex(req, res, next) {
        try {
            const result = await this._service.findFollowed(req.query, req.user);
            res.json(result);
        } catch(e) {
            return next(this.buildError(e));
        }
    }

    async advancedSearch(req, res, next) {
        try {
            const result = await this._service.advancedFind(req.body, req.user);
            res.json(result);
        } catch(e) {
            return next(this.buildError(e));
        }
    }

    async like(req, res, next) {
        try {
            await this._service.like(req.params._id, req.user);
            res.json(this.buildMessage('0015'));
        } catch(e) {
            return next(this.buildError(e));
        }
    }
    async unlike(req, res, next) {
        try {
            await this._service.unlike(req.params._id, req.user);
            res.json(this.buildMessage('0015'));
        } catch(e) {
            return next(this.buildError(e));
        }
    }
    async watched(req, res, next) {
        try {
            await this._service.updateWatched(req.params, req.user);
            res.json(this.buildMessage('0015'));
        } catch(e) {
            return next(this.buildError(e));
        }
    }
};
