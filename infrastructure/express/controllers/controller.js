const status = require('http-status');
const messageDictionary = require('../../utils/message-dictionary');

module.exports = class Controller {
    constructor(service) {
        this._service = service;
    }

    async create(req, res, next) {
        try {
            const result = await this._service.create(req.body, req.user);
            res.json(result);
        } catch(e) {
            return next(this.buildError(e));
        }
    }

    async update(req, res, next) {
        try {
            await this._service.update(req.params._id, req.body);
            res.json(this.buildMessage('0015'));
        } catch(e) {
            return next(this.buildError(e));
        }
    }

    async destroy(req, res, next) {
        try {
            await this._service.remove(req.params._id);
            res.json(this.buildMessage('0015'));
        } catch(e) {
            return next(this.buildError(e));
        }
    }

    async softDelete(req, res, next) {
        try {
            await this._service.softDelete(req.params._id);
            res.json(this.buildMessage('0015'));
        } catch(e) {
            return next(this.buildError(e));
        }
    }

    async index(req, res, next) {
        try {
            const result = await this._service.find(req.query, req.user);
            res.json(result);
        } catch(e) {
            return next(this.buildError(e));
        }
    }

    async show(req, res, next) {
        try {
            if(!req.params._id.match(/^[0-9a-fA-F]{24}$/)) {
                return next(this.buildError('0016', status.NOT_FOUND));
            }
            const result = await this._service.findById(req.params._id, req.user);
            if (!result) {
                return next(this.buildError('0016', status.NOT_FOUND));
            }
            res.json(result);
        } catch(e) {
            return next(this.buildError(e));
        }
    }

    buildError(...args) {
        let e = args[0];
        if (typeof args[0] === 'string') {
            e = new Error(args[0]);
        }

        if (typeof args[1] === 'number') {
            e.httpStatus = args[1];
        } else if(args[1]) {
            e.originError = args[1];
        }

        if (args[2]) {
            e.originError = args[2];
        }

        return e;
    }

    buildMessage(code) {
        return {
            code,
            message: messageDictionary[code]
        };
    }
};
