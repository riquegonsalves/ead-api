const Service = require('./service');
const TrackModel = require('../models/track.model');

module.exports = class TrackService extends Service {
    constructor() {
        super(TrackModel);
    }

    async find(query) {
        const filterOptions = this.filterOptions(query);
        const tracks = await this._model.find(query, null, filterOptions).populate('videos');
        const total = await this._model.countDocuments(query);
        return {
            docs: tracks,
            total
        };
    }

    findById(_id) {
        return this._model.findById(_id).populate('videos');
    }
};
