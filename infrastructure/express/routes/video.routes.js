const VideoController = require('../controllers/video.controller');
const verifyApiToken = require('../middlewares/verify-api-token');

const controller = new VideoController();

module.exports = (router) => {
    router.route('/videos')
    .all(verifyApiToken)
    .get(controller.index.bind(controller))
    .post(controller.create.bind(controller));

    router.route('/videos/followed')
    .all(verifyApiToken)
    .get(controller.followedIndex.bind(controller));

    router.route('/videos/search')
    .all(verifyApiToken)
    .post(controller.advancedSearch.bind(controller));

    router.route('/videos/:_id')
    .all(verifyApiToken)
    .put(controller.update.bind(controller))
    .get(controller.show.bind(controller))
    .delete(controller.softDelete.bind(controller));

    router.route('/videos/:_id/like')
    .all(verifyApiToken)
    .post(controller.like.bind(controller))
    .delete(controller.unlike.bind(controller));

    router.route('/videos/:_id/watched/:watched')
    .all(verifyApiToken)
    .post(controller.watched.bind(controller));
};
