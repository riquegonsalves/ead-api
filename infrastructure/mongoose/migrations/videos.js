load('./utils.js');

function parseVideo(video) {

    let allowed_roles = ['PREMIUM_USER'];

    if(video.isBasic) {
        allowed_roles.push('BASIC_USER');
    }

    return {
        _id: video._id,
        title: video.title,
        slug: video.slug,
        author: video.creator,
        summary: video.comments[0].content,
        url: `https://player.vimeo.com/video/${video.video}`,
        allowed_roles,
        deleted: false,
        created_at: video.createdAt,
        duration: video.duration
    }

}

function videoMigration() {
    const category = db.categories_legacy.findOne();
    let countTopics = 0;
    category.subCategories.forEach(modality => {
        const topicsCursor = db.topics_legacy.find({ _id: { $in: modality.topics } });
        countTopics += iterate(topicsCursor, video => {
            db.videos.insert(parseVideo(video));
        });
    });

    print('Videos loaded: ' + countTopics);
}
