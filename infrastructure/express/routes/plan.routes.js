const Controller = require('../controllers/controller');
const PlanService = require('../../../business/plan.service');

const controller = new Controller(new PlanService());

module.exports = (router) => {
    router.route('/plans')
    .get(controller.index.bind(controller));
};
