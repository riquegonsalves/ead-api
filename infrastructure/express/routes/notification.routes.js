const MessageController = require('../controllers/message.controller');
const verifyApiToken = require('../middlewares/verify-api-token');

const controller = new MessageController();

module.exports = (router) => {
    router.route('/notifications')
    .all(verifyApiToken)
    .get(controller.listNotifications.bind(controller));

    router.route('/notifications/:_id')
    .all(verifyApiToken)
    .put(controller.markAsRead.bind(controller));
};
