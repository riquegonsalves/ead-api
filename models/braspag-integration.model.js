const { mongoose } = require('../infrastructure/mongoose');

const BraspagIntegrationSchema = new mongoose.Schema({
    order: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Order',
        required: true
    },
    request: {
        type: mongoose.Schema.Types.Mixed,
        required: true
    },
    response: {
        type: mongoose.Schema.Types.Mixed,
        required: true
    },
    status: {
        type: Number,
        required: true
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

BraspagIntegrationSchema.statics.register = function(order, request, response, status) {
    const creditCard = request["Payment"]["CreditCard"];
    if (creditCard && typeof creditCard["CardNumber"] === 'string' ) {
        const begin = creditCard["CardNumber"].slice(0, 6);
        const end = creditCard["CardNumber"].slice(-4);
        creditCard["CardNumber"] = `${begin}*****${end}`;
    }

    return this.create({ order, request, response, status })
};

module.exports = mongoose.model('BraspagIntegration', BraspagIntegrationSchema);
