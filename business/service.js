module.exports = class Service {
    constructor(model) {
        if (model) {
            this._model = model;
        }
    }

    create(body) {
        return this._model.create(body);
    }

    update(_id, body) {
        return this._model.update(
            { _id },
            { $set: body},
            { runValidators: true }
        );
    }

    remove(_id) {
        return this._model.deleteOne({ _id});
    }

    softDelete(_id) {
        return this.update(_id, {
            deleted: true
        });
    }

    find(query) {
        return this._model.find(query);
    }

    findOne(query) {
        return this._model.findOne(query);
    }

    findById(_id) {
        return this._model.findById(_id);
    }

    filterOptions(query) {
        let { page, length, sort } = query;
        page = page ? parseInt(page) : 1;
        length = length ? parseInt(length) : 4;
        sort = sort ? `${sort} -created_at` : '-created_at';
        delete query.page;
        delete query.length;
        delete query.sort;

        return {
            skip: (page - 1) * length,
            limit: length,
            sort
        };
    }
};
