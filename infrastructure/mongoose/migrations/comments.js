load('./utils.js');

function parseComment(comment, origin_comment = null) {
    let answers = [];
    if(origin_comment == null) {
         answers = comment.replies.map(c => c._id);
    }
    return {
        _id: comment._id,
        author: comment.user,
        body: comment.content,
        video: comment.video,
        origin_comment,
        answers,
        created_at: comment.createdAt
    }
}

function insertComment(comment) {
    db.comments.insert(parseComment(comment));
    let inserted = 1;
    let replies = [];
    comment.replies.forEach(reply => {
        reply.video = comment.video;
        replies.push(parseComment(reply, comment._id));
    });
    db.comments.insertMany(replies);
    return inserted + replies.length;;
}

function commentsMigration() {
    const cursor = db.topics_legacy.find({video: {$ne: ''}});
    let totalComments = 0;
    iterate(cursor, (video) => {
        let comments = video.comments.slice(1);
        comments.forEach(comment => {
            comment.video = video._id;
            totalComments += insertComment(comment);
        });
    });

    print('Comments loaded: ' + totalComments);
}
