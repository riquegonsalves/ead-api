const PostController = require('../controllers/post.controller');
const verifyApiToken = require('../middlewares/verify-api-token');

const controller = new PostController();

module.exports = (router) => {
    router.route('/topics/:topic/posts')
    .all(verifyApiToken)
    .get(controller.index.bind(controller));

    router.route('/posts')
    .all(verifyApiToken)
    .post(controller.create.bind(controller));

    router.route('/posts/:_id')
    .all(verifyApiToken)
    .put(controller.update.bind(controller))
    .get(controller.show.bind(controller))
    .delete(controller.softDelete.bind(controller));
};
