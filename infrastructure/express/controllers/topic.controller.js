const Controller = require('./controller');
const TopicService = require('../../../business/topic.service');
const status = require('http-status');

module.exports = class TopicController extends Controller {
    constructor() {
        super(new TopicService());
    }
};
