const { mongoose } = require('../infrastructure/mongoose');

const CommentLikeSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    comment: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Comment',
        required: true
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

CommentLikeSchema.statics.registerLike = async function(comment, user) {
    const comment_like = await this.findOne({ user, comment });
    if (!comment_like) {
        await this.create({ user, comment });
    } else {
        throw new Error('0024');
    }
};

module.exports = mongoose.model('CommentLike', CommentLikeSchema);
