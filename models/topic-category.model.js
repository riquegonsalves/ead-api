const { mongoose } = require('../infrastructure/mongoose');

const TopicCategorySchema = new mongoose.Schema({
    _id: {
        type: String,
        trim: true,
        maxlength: 50,
        required: true
    },
    name: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('TopicCategory', TopicCategorySchema);
