const CommentController = require('../controllers/comment.controller');
const verifyApiToken = require('../middlewares/verify-api-token');

const controller = new CommentController();

module.exports = (router) => {
    router.route('/comments')
    .all(verifyApiToken)
    .get(controller.index.bind(controller))
    .post(controller.create.bind(controller));

    router.route('/comments/:_id')
    .all(verifyApiToken)
    .put(controller.update.bind(controller))
    .get(controller.show.bind(controller))
    .delete(controller.softDelete.bind(controller));

    router.route('/comments/:_id/like')
    .all(verifyApiToken)
    .post(controller.like.bind(controller));
};
