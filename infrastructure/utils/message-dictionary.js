module.exports = {
    // O código 0001 está reservado para mensagens dinâmicas criadas pelos frameworks auxiliares
    '0002': 'Falha interna no servidor, contate o suporte técnico',
    '0003': 'E-mail não verificado',
    '0004': 'Não foi possível enviar e-mail para o endereço informado',
    '0005': 'Somente um parâmetro deve ser verificado por vez',
    '0006': 'Somente a disponibilidade de username ou email pode ser verificada',
    '0007': 'Usuário não encontrado!',
    '0008': 'E-mail já verificado',
    '0009': 'Token ausente',
    '0010': 'Token inválido',
    '0011': 'E-mail não informado no corpo da requisição',
    '0012': 'Nova senha não informada no corpo da requisição',
    '0013': 'Endereço não encontrado a partir do CEP informado',
    '0014': 'Usuário ou senha inválidos',
    '0015': 'Operação realizada com sucesso',
    '0016': 'Recurso não encontrado',
    '0017': 'Versão de token inválida, é necessário realizar login novamente',
    '0018': 'Usuário não logado, é necessário realizar login novamente',
    '0019': 'Usuário já deu like nesse vídeo',
    '0020': 'Usuário já segue esse Sensei',
    '0021': 'Transação rejeitada para esse pedido',
    '0022': 'Plano não encontrado',
    '0023': 'Somente usuários premium podem comentar vídeos',
    '0024': 'Usuário já deu like nesse comentário',
    '0025': 'Busca não permitida para o seu perfil de usuário',
    '0026': 'Vídeo não encontrado',
    '0027': 'Tempo inválido para o vídeo informado',
    '0028': 'Trilha não encontrada',
    '0029': 'Usuário já iniciou essa trilha',
    '0030': 'Usuário ainda não deu like nesse video',
    '9999': 'Falha interna no servidor, contate o suporte técnico'
};
