FROM node:8.14-slim

WORKDIR /usr/src/app

COPY package.json .
COPY package-lock.json .

RUN apt-get update
RUN apt-get install -y build-essential
RUN apt-get install -y python

RUN npm install

COPY . .

EXPOSE 4000

CMD [ "npm", "start"]
