const { mongoose } = require('../infrastructure/mongoose');

const UserIpSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    ip: {
        type: String,
        trim: true,
        maxlength: 100,
        required: true
    },
    city: {
        type: String,
        trim: true,
        maxlength: 100
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});

UserIpSchema.statics.registerIP = async function(user, ip) {
    const user_ip = await this.findOne({ user: user._id, ip });
    if (!user_ip) {
        await this.create({ user, ip });
    }
};

module.exports = mongoose.model('UserIp', UserIpSchema);
